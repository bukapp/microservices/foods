const mongoose = require("mongoose")

const schema = mongoose.Schema({
    restaurant: String,
    name: String,
    extras: [{
        name: String,
        price: Number
    }],
    choices: [{
        name: String,
        options: [{
            name: String, 
            price: Number
        }]
    }],
    price: Number,
    tags: [String],
    description: String,
    image: String,
})

const repository = mongoose.model("Foods", schema)

export {repository as FoodsRepository}