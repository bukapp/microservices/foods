import express, { Express, Request, Response, Router, NextFunction } from 'express';
import middleware from '../modules/middleware';
import { FoodsRepository } from '../models/Foods'
import axios from 'axios'


var router: Router = express.Router()

/**** SECURE ZONE ****/
router.use(middleware)

/* GET all foods */
router.get('/', async function (req: Request, res: Response, next: NextFunction) {
    const foods = await FoodsRepository.find();
    if (foods != null) {
        res.send(foods);
    }
    else {
        res.sendStatus(404);
    }
});
/* POST a food */
router.post('/', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const user_restaurant: any = await axios.get(`http://apigateway:85/restaurants/user/${req.body.user.id}`, {
            headers: {
                'x-access-token': req.header('x-access-token') || ""
            }
        })
        const post = new FoodsRepository({
            restaurant: user_restaurant.data._id,
            name: req.body.name,
            extras: req.body.extras,
            choices: req.body.choices,
            price: req.body.price,
            image: req.body.image,
            description: req.body.description,
            tags: req.body.tags
        })
        await post.save()
        res.send(post)

    } catch (error) {
        console.log(error)
        res.status(400).send({ error: "No restaurant to add food to" })
    }
});
/* GET a food */
router.get('/:id', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const foods = await FoodsRepository.findOne({ _id: req.params.id });
        if (foods != null) {
            res.send(foods);
        }
        else {
            res.sendStatus(404);
        }
    } catch (error) {
        console.log(error)
        res.sendStatus(500)
    }
});
/* GET all food of a restaurant */
router.get('/restaurant/:id', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const foods = await FoodsRepository.find({ restaurant: req.params.id });
        if (foods != null) {
            res.send(foods);
        }
        else {
            res.sendStatus(404);
        }
    } catch (error) {
        console.log(error)
        res.sendStatus(500)
    }
});

/* UPDATE a food */
router.put('/:id', async function (req: Request, res: Response, next: NextFunction) {
    try {
        const food = await FoodsRepository.findOne({ _id: req.params.id })

        food.restaurant = req.body.restaurant || food.restaurant;
        food.name = req.body.name || food.name;
        food.extras = req.body.extras || food.extras;
        food.choices = req.body.extras || food.choices;
        food.price = req.body.price || food.price;
        food.image = req.body.image || food.image;
        food.description = req.body.description || food.description;
        food.tags = req.body.tags || food.tags;

        await food.save()
        if (food != null) {
            res.send(food);
        }
        else {
            res.sendStatus(404);
        }
    } catch (error) {
        console.log(error)
        res.sendStatus(500)
    }
});
/* DELETE a food */
router.delete('/:id', async function (req: Request, res: Response, next: NextFunction) {
    try {
        await FoodsRepository.deleteOne({ _id: req.params.id })
        res.status(204).send()
    } catch (error) {
        console.log(error)
        res.status(404).send({ error: "Food doesn't exist!" })
    }
});

export default router;
